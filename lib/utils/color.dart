import 'package:flutter/material.dart';

class MyCustomColors {
  
  static final lightTheme = ThemeData.light().copyWith(
    scaffoldBackgroundColor: const Color.fromRGBO(238, 238, 243, 1),
    backgroundColor: const Color.fromRGBO(238, 238, 243, 1),
    primaryColor: const Color.fromRGBO(66, 99, 209, 1),
    accentColor: const Color.fromRGBO(46, 61, 94, 1)
  );

  static final darkTheme = ThemeData.dark().copyWith(
    backgroundColor: const Color.fromRGBO(27, 35, 54, 1),
    primaryColor: const Color.fromRGBO(66, 99, 209, 1)
  );
}
