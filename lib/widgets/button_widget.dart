import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/theme_provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ButtonWidget extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final bool actionWaitColor;

  ButtonWidget({
	  required this.text,
	  required this.onPressed, 
	  required this.actionWaitColor, 
	  Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    
	final themeProvider = Provider.of<ThemeProvider>(context);
    
	return InkWell(
		onTap: onPressed,
		child: Container(
			height: ScreenUtil().setHeight(30.0),
			decoration: BoxDecoration(
				color: Theme.of(context).primaryColor,
				borderRadius: BorderRadius.circular(12.0),
				boxShadow: [
					BoxShadow(
						color: actionWaitColor ? Theme.of(context).primaryColor.withOpacity(0.5) : Theme.of(context).primaryColor,
						spreadRadius: 0,
						blurRadius: 8.0,
						offset: Offset(0, 2)
					)
				]
			),
			child: Center(
				child: Text(
					this.text,
					style: TextStyle(
						color: Colors.white
					),
				),
			),
		),
	);
  }
}
