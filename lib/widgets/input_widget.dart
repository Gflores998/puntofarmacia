import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InputWidget extends StatefulWidget{
  final ValueChanged<String> onSubmit;

  final String hintText;
  final Widget suffixIcon;
  final bool obscureText;
  final TextInputType keyboardType;
  final TextEditingController textController;
  final bool needValidation;
  final bool needValueInput;

  InputWidget({
    required this.textController,
    required this.hintText,
    this.obscureText = false,
    required this.suffixIcon,
    this.keyboardType = TextInputType.text,
    this.needValidation = false,
    this.needValueInput = false,
    required this.onSubmit,
    Key? key
  }) : super(key: key);

  @override
  _InputWidgetState createState() => _InputWidgetState();

}

class _InputWidgetState extends State<InputWidget> {
  late String _value;

  @override
  Widget build(BuildContext context) {

    return Container(
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).primaryColor),
        borderRadius: BorderRadius.circular(12.0),
      ),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: TextFormField(
        obscureText: this.widget.obscureText,
        keyboardType: this.widget.keyboardType,
        controller: this.widget.textController,
        maxLines: null,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Por favor agrege la información';
          }
          return null;
        },
        onFieldSubmitted: (filter) {
          if (widget.needValueInput) {
            setState(() {
              _value = filter;
            });
            widget.onSubmit(filter);
          }
        },
        decoration: InputDecoration(
          hintText: this.widget.hintText,
          hintStyle: TextStyle(
            fontSize: 12.0,
            color: Colors.black38,
            fontWeight: FontWeight.w600
          ),
          suffixIcon: this.widget.suffixIcon == null ? null : widget.suffixIcon,
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          )
        ),
      )
    );
  }


}