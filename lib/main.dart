import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:proyecto/bloc/mi_ubicacion/mi_ubicacion_bloc.dart';
import 'package:proyecto/providers/theme_provider.dart';
import 'package:proyecto/services/crud_service.dart';
import 'package:proyecto/utils/color.dart';
import './controllers/loading.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
    create: (context) => ThemeProvider(),
    builder: (context, _) {
      final themeProvider = Provider.of<ThemeProvider>(context);
      return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: ( _ ) => CrudService()),
          BlocProvider(create: (_) => MiUbicacionBloc())
        ],
        child: ScreenUtilInit(
          builder: () => MaterialApp(
            debugShowCheckedModeBanner: false,
            themeMode: themeProvider.themeMode,
            theme: MyCustomColors.lightTheme,
            darkTheme: MyCustomColors.darkTheme,
            home: LoadingPage(),
          ),
        ),
      );
    },
  );
}
