import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:proyecto/controllers/loading.dart';
import 'package:proyecto/pages/home.dart';
import 'package:proyecto/widgets/button_widget.dart';
import 'package:proyecto/helpers/helpers.dart';

class GPSAccessPage extends StatefulWidget {
  @override
  State<GPSAccessPage> createState() => _GPSAccessPageState();
}

class _GPSAccessPageState extends State<GPSAccessPage> with WidgetsBindingObserver {

  bool popUp = false;

  @override
  void initState() {
    
    WidgetsBinding.instance!.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if ( state == AppLifecycleState.resumed && !popUp) {
      if (await Permission.location.isGranted) {
        Navigator.pushReplacement(context, navegateMapFadeIn(context, LoadingPage()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Es necesario habilitar el GPS para usar esta Aplicación', 
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.bold,
                fontSize: 15.0
              )
            ),
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.all(16.0), 
              child: ButtonWidget(
              text: 'Habilitar', 
              onPressed: () async {
                popUp = true;

                final status = await Permission.location.request();
                await accessGPS(status);

                popUp = false;
              }, 
              actionWaitColor: true
            )
            )
          ]
        ),
      ),
    );
  }

  Future accessGPS(PermissionStatus status) async {
    print( status );

    switch ( status ) {
      case PermissionStatus.granted:
        
        await Navigator.pushReplacement(context, navegateMapFadeIn(context, LoadingPage()));
        break;
      
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
      case PermissionStatus.limited:
      // case PermissionStatus.permanentlyDenied:
      openAppSettings();
    }
  }
}
