import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart' as Geolocator;
import 'package:permission_handler/permission_handler.dart';
import 'package:proyecto/helpers/helpers.dart';
import 'package:proyecto/controllers/gps_access.dart';
import 'package:proyecto/pages/home.dart';

class LoadingPage extends StatefulWidget {
  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> with WidgetsBindingObserver {

	@override
	void initState() {
		WidgetsBinding.instance!.addObserver(this);
		super.initState();
	}

	@override
	void dispose() {
		WidgetsBinding.instance!.removeObserver(this);
		super.dispose();
	}

	@override
	void didChangeAppLifecycleState(AppLifecycleState state) async {
		if ( state == AppLifecycleState.resumed) {
			if(await Geolocator.GeolocatorPlatform.instance.isLocationServiceEnabled()) {
      			Navigator.pushReplacement(context, navegateMapFadeIn(context, HomePage()));
			}
		}
	}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
            future: checkGPSLocation(context),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
				if (snapshot.hasData) {
					return Center(child: Text(snapshot.data));
				} else {
					return const Center(
						child: CircularProgressIndicator(
						strokeWidth: 2,
						),
					);
				}
            }
        )
    );
  }

  Future checkGPSLocation(BuildContext context) async {
    final permissionGPSIsOk =  await Permission.location.isGranted;
    final GPSIsActive = await Geolocator.GeolocatorPlatform.instance.isLocationServiceEnabled();

    if (permissionGPSIsOk && GPSIsActive) {
      Navigator.pushReplacement(context, navegateMapFadeIn(context, HomePage()));
      return '';
    } else if(!permissionGPSIsOk) {
      Navigator.pushReplacement(context, navegateMapFadeIn(context, GPSAccessPage()));
      return 'Es necesario habilitar el uso de GPS para esta app';
    } else {
      return 'Es necesario activar el GPS';
    }
    
    
  }
}
