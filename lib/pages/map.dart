import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:proyecto/bloc/mi_ubicacion/mi_ubicacion_bloc.dart';

class MapPage extends StatefulWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> with WidgetsBindingObserver {

  @override
  void initState() {

    context.read<MiUbicacionBloc>().startFollowing();
    
    super.initState();
  }

  @override
  void dispose() {
    // TODO: Make this stop when change page
    // context.read<MiUbicacionBloc>().cancelFollowing();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MiUbicacionBloc, MiUbicacionState> (
        builder: (_, state) => createMap( state )
      )
    );
  }

  Widget createMap (MiUbicacionState state) {
    if ( !state.hasLastLocation ) return Center(child: Text('Ubicando..'));

    print('asd ${ state.myLocation }');

    final cameraPosition = CameraPosition(target: state.myLocation, zoom: 15);

    return GoogleMap(
      initialCameraPosition: cameraPosition,
      myLocationEnabled: true,
    );
  }

}
