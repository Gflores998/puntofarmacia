import 'package:flutter/material.dart';
import 'package:proyecto/widgets/input_widget.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {

  final nameCtrl = TextEditingController();

  // TODO: Change To DB
  final items = List.generate(10, (index) => 'Favorito $index ' );

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 10,
              ),
              Center(
                child: InputWidget(
                    textController: nameCtrl,
                    keyboardType: TextInputType.text,
                    hintText: 'Buscar Documento',
                    suffixIcon: Icon(Icons.search, color: Theme.of(context).accentColor), 
                    onSubmit: (String value) {  },
                ),
              ),
              SizedBox(
                height: 10,
              ),
              listViewOfFavorites()
            ],
          ),
        ),
      ),
    );
  }

  listViewOfFavorites() {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: items.length,
      itemBuilder: (_, int index) {
        return favoriteItem(items[index]);
      }, 
      separatorBuilder: (_, int index) {
        return SizedBox(
          height: 10.0,
        );
      }
    );
  }

  Widget favoriteItem(String title) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: Icon(Icons.star, color: Colors.yellow,),
              title: Text(title, style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor
              )),
              subtitle: Text('Subtitulo - Opcion'),
            )
          ],
        ),
      ),
    );
  }

}
