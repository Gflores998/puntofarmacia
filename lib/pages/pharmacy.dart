import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PharmacyPage extends StatefulWidget {
  const PharmacyPage({Key? key}) : super(key: key);

  @override
  _PharmacyPageState createState() => _PharmacyPageState();
}

class _PharmacyPageState extends State<PharmacyPage> {
  final List listOfCategories = [
    'Medicamentos',
    'Suplementos',
    'Higiene',
    'Vitaminas'
  ];

  final items = List.generate(10, (index) => 'Producto ${ index + 1 }' );

	@override
	Widget build(BuildContext context) {
		return SingleChildScrollView(
			child: SafeArea(
				child: Container(
					padding: EdgeInsets.symmetric(horizontal: 16.0),
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						mainAxisSize: MainAxisSize.min,
						children: [
							SizedBox(
							height: 10,
							),
							Center(
							child: pharmacyInfo(),
							),
							SizedBox(
							height: 10,
							),
							Text(
							'Categorías',
							style: TextStyle(
								fontSize: 21.0,
								color: Theme.of(context).primaryColor,
								fontWeight: FontWeight.w600),
							),
							SizedBox(height: 10.0),
							Container(height: 60.0, child: categoryInfo()),
							SizedBox(height: 10.0),
							Text(
								'Productos',
								style: TextStyle(
									fontSize: 21.0,
									color: Theme.of(context).primaryColor,
									fontWeight: FontWeight.w600
								),
							),
							listViewOfProducts()
						]
					)
				)
			)
		);
  	}

  Widget categoryInfo() {
    return ListView.separated(
        separatorBuilder: (_, int index) {
          return SizedBox(width: 10.0);
        },
        scrollDirection: Axis.horizontal,
        itemCount: listOfCategories.length,
        itemBuilder: (_, index) {
          return Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Container(
                  height: ScreenUtil().setHeight(140.0),
                  width: ScreenUtil().setWidth(140.36),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(224, 230, 255, 1),
                    borderRadius: BorderRadius.circular(8.0),
                    image: DecorationImage(
                      image: AssetImage('assets/images/logo.png'),
                    ),
                  )),
              Container(
                padding: EdgeInsets.all(8.0),
                width: ScreenUtil().setWidth(140.36),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      listOfCategories[index],
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontSize: 15.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  
                  ],
                ),
              )
            ],
          );
        });
  }

  listViewOfProducts() {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: items.length,
      itemBuilder: (_, int index) {
        return productItem(items[index]);
      }, 
      separatorBuilder: (_, int index) {
        return SizedBox(
          height: 10.0,
        );
      }
    );
  }

  Widget pharmacyInfo() {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 20.0),
            leading: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: 85, minWidth: 85, maxHeight: 105, maxWidth: 105),
              child: Image.asset(
                'assets/images/logo.png',
                fit: BoxFit.cover,
              ),
            ),
            title: Text('Farmacia Uno',
                style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0)),
            subtitle: Text(
              'Otra información relevante de la de la farmacia, está opción deberá mostrar mas de una linea de texto',
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
          )
        ],
      ),
    );
  }

  Widget productItem(String title) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: Icon(Icons.call_made_rounded, color:Theme.of(context).primaryColor),
              title: Text(title, style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor
              )),
              subtitle: Text('Descripción del producto'),
            )
          ],
        ),
      ),
    );
  }
}
