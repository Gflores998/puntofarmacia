import 'package:flutter/material.dart';
import '../pages/map.dart';
import '../pages/pharmacy.dart';
import '../pages/product.dart';
import '../pages/favorite.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var pageList = [
    const MapPage(),
    const FavoritePage(),
    const PharmacyPage(),
    const ProductPage()
  ];
  int indexPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          centerTitle: true,
          title: Text('Punto Farmacia'),
          backgroundColor: Theme.of(context).primaryColor
        ),
      ),
      body: pageList[indexPage],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).backgroundColor,
        selectedItemColor: Theme.of(context).accentColor,
        iconSize: 20,
        currentIndex: indexPage,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: true,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        onTap: (int currentIndex) {
          setState(() {
            indexPage = currentIndex;
          });
        },
        // ignore: prefer_const_literals_to_create_immutables
        items: [
          // ignore: prefer_const_constructors
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Inicio'),
          // ignore: prefer_const_constructors
          BottomNavigationBarItem(icon: Icon(Icons.star), label: 'Favoritos'),
          // ignore: prefer_const_constructors
          BottomNavigationBarItem(icon: Icon(Icons.medical_services), label: 'Farmacias'),
          // ignore: prefer_const_constructors
          BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Productos'),
        ],
      ),
    );
  }
}
