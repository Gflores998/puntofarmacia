part of 'mi_ubicacion_bloc.dart';

@immutable
class MiUbicacionState {
  final bool following;
  final bool hasLastLocation;
  final LatLng myLocation;

  MiUbicacionState({
    this.following = true,
    this.hasLastLocation = false,
    required this.myLocation
  });

  MiUbicacionState copyWith({
    required bool following,
    required bool hasLastLocation,
    required LatLng myLocation
  }) => new MiUbicacionState(
    following: following,
    hasLastLocation:hasLastLocation,
    myLocation: myLocation
  ) ;


}

