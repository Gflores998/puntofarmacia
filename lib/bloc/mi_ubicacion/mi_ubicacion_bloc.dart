import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart' as Geolocator;
import 'package:meta/meta.dart';

part 'mi_ubicacion_event.dart';
part 'mi_ubicacion_state.dart';

class MiUbicacionBloc extends Bloc<MiUbicacionEvent, MiUbicacionState> {

  MiUbicacionBloc() : super(MiUbicacionState(myLocation: LatLng(90.0 , -89.1285224)));

  late StreamSubscription<Geolocator.Position> _streamSubscription;

  void startFollowing() {
  
    _streamSubscription = Geolocator.GeolocatorPlatform.instance.getPositionStream(
        desiredAccuracy: Geolocator.LocationAccuracy.high,
        distanceFilter: 10
      ).listen((Geolocator.Position position) { 
        final newLocation = new LatLng(position.altitude, position.longitude);
        add(OnLocationChanged(newLocation));
      });
  }

  void cancelFollowing() {
    _streamSubscription.cancel();
  }

  @override
  Stream<MiUbicacionState> mapEventToState(MiUbicacionEvent event) async* {
    if ( event is OnLocationChanged ) {
      yield state.copyWith(
        following: true,
        hasLastLocation: true, 
        myLocation: event.myLocation);
    }
  }


}
