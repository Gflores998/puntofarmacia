part of 'mi_ubicacion_bloc.dart';

@immutable
abstract class MiUbicacionEvent {}


class OnLocationChanged extends MiUbicacionEvent {
  
  final LatLng myLocation;

  OnLocationChanged(this.myLocation);
}