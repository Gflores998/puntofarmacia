# Proyecto - Punto Farmacia 

Proyecto para la materia Técnicas de Calidad de Software 01

## Grupo de Trabajo
A continuación, se detalla el equipo de trabajo:

- Alfaro Ayala, Rene Antonio
- Alvarado Estrada, José Eduardo
- Caballero Contreras, Karina Yamilet
- Flores Mejía, Gerardo Alexis
- Garcia Ayala, Cristian Vladimir

## Plugins utilizados para este proyecto:
A continuación, se detalla el listado y versión de los plugins utilizados en el proyecto
hasta el momento:

``` Terminal
geolocator: ^7.6.2
permission_handler: ^8.1.6
flutter_bloc: ^7.3.0
google_maps_flutter: ^2.0.10
dio: ^4.0.0
animate_do: ^2.0.0
provider: ^6.0.0
flutter_screenutil: ^5.0.0+2
```

## Pantallas actuales de la aplicación:
<img src="./pantallas/1.jpeg" width="150" title="Pantalla de solicitud de permiso">


<img src="./pantallas/2.jpeg" width="150" title="Pantalla de solicitud de permiso">

<img src="./pantallas/3.jpeg" width="150" title="Activar GPS">

<img src="./pantallas/4.jpeg" width="150" title="Activando">


<img src="./pantallas/5.jpeg" width="150" title="Pantalla de inicio">

<img src="./pantallas/6.jpeg" width="150" title="Pantalla de favoritos">

<img src="./pantallas/7.jpeg" width="150" title="Detalle">